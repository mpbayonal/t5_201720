package view;

import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IList;
import model.vo.VOBlock;


public class STSManagerView {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		
		
		while(!fin){
			
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					Controller.loadTrips();
					
					break;
				case 2:
					Controller.generarTablaResumen();
					break;
				case 3:
					Controller.generarColaPrioridad();
					break;
				case 4:
					IList<VOBlock> rutasLista = Controller.darListaOrdenadaPrioridad();
					Iterator<VOBlock> iterador= rutasLista.iterator();
					
					while(iterador.hasNext()) 
					{
						VOBlock elemento = iterador.next();
						System.out.println("Id Ruta: "+ elemento.getIdRuta() + " Prioridad: " + elemento.getTotalBlock());
					
					}
					
					break;
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
	
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4-5----------------------");
		
		System.out.println("1. Cree una nueva coleccion de viajes (data/trips.txt)");
		System.out.println("2. Generar una tabla de resumen a partir de la coleccion de viajes");
		System.out.println("3. Generar una cola de prioridad basada en la cantidad de veces que una ruta aparece");
		System.out.println("4. mostrar todas las rutas y sus respectivas prioridad");
		
		System.out.println("5. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
		
	}
}
