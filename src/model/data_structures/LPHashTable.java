package model.data_structures;


import java.util.ArrayList;



public class LPHashTable<K,V> {

		private int m;
		private int size;
		
		 private ArrayList<HashNode<K,V>> array;
		
		public LPHashTable(int pm) {
			array = new ArrayList<>();
	        m = pm;
	        size = 0;
	 
	      
	        for (int i = 0; i < m; i++) {
	            array.add(null);
	        }		
		}
		
		
		public void put(K key, V value) {
			
	        int i = getIndex(key);
	        HashNode<K, V> head = array.get(i);
	 
	       
	        while (head != null)
	        {
	            if (head.key.equals(key))
	            {
	                head.value = value;
	                return;
	            }
	            head = head.next;
	        }
	 
	      

	        size++;
	        head = array.get(i);
	        HashNode<K, V> newNode = new HashNode<K, V>(key, value);
	        newNode.next = head;
	        array.set(i, newNode);
	 
	        
	        if (linearProbing())
	        {
	        		rehash();
	        }
			
		}
		
		public  V  get(K key) 
		{
			 
	        int i = getIndex(key);
	        HashNode<K,V> head = array.get(i);
	 
	        
	        while (head != null)
	        {
	            if (head.key.equals(key))
	                return head.value;
	            head = head.next;
	        }
	 
	        
	        return null;
		}
		public boolean delete(K key) {
			
	        int i = getIndex(key);
	 
	       
	        HashNode<K,V> head = array.get(i);
	 
	        
	        HashNode <K,V> prev = null;
	        while (head != null)
	        {
	           
	            if (head.key.equals(key))
	                break;
	 
	            
	            prev = head;
	            head = head.next;
	        }
	 
	       
	        if (head == null) {
	            return false;
	        }
	        
	        size--;
	 
	        
	        if (prev != null)
	            prev.next = head.next;
	        else
	           array.set(i, head.next);
	 
	        return true;
		}
		public int size() {
			return size;
		}

		public int getIndex( K key) {
			
			 int hashCode = key.hashCode();
		     int index = hashCode % m;
		     return index;
			
			
		}
		 public boolean isEmpty() { 
			 return size == 0; 
			 }
		 public boolean linearProbing() {
			 boolean r= false;
			 if((1.0*size)/m >= 0.75){
				 r=true;
			 }
			 return r;
		 }
		 public void rehash() {
			 
			 ArrayList<HashNode<K, V>> temp = array;
	         array = new ArrayList<>();
	         m = darPrimoSiguiente(2*m);
	         size = 0;
	         for (int j = 0; j < m; j++)
	             array.add(null);

	         for (HashNode<K, V> headNode : temp)
	         {
	             while (headNode != null)
	             {
	                 put(headNode.key, headNode.value);
	                 headNode = headNode.next;
	             }
	         }
		 }
		 public int darPrimoSiguiente(int i) {
			int r=i;
			 
			 while(true) {
				 r++;
				 if(esPrimo(r)) {
					 break;
				 }
			 }
			 
			 return r;
		 }
		 public boolean esPrimo(int n) {
		 for(int i=2;i<n;i++) {
		        if(n%i==0)
		            return false;
		    }
		    return true;
		 }
	}


