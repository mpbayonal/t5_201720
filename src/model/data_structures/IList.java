package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T extends Comparable<T>> extends Iterable<T> {

	Integer getSize();
	
	void next() throws Exception;
	
	void previous() throws Exception;
	
	T getElement();
		
	T getElementAtK(int k) throws Exception;
	
	boolean addInOrder(T elementToAdd) throws Exception;
	
	boolean add(T elementToAdd) throws Exception;
	
	boolean addAtEnd(T elementToAdd);
	
	boolean addAtK(T elementToAdd, int k ) throws Exception;
	
	boolean delete();
	
	boolean deleteAtK(int k);
	
	
	

}
