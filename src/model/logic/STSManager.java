package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import model.vo.VOBlock;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.PriorityQueue;


public class STSManager
{
	PriorityQueue<VOBlock> colaPrioridad;

	private DoubleLinkedList<VOTrip> trips;
	public DoubleLinkedList<VOBlock> resumenList;



	public STSManager() 
	{


		trips = new DoubleLinkedList<VOTrip>();
		colaPrioridad = new PriorityQueue<VOBlock>();


	}

	
	
	
	public DoubleLinkedList<VOBlock> punto6() throws Exception
	{
		DoubleLinkedList<VOBlock> r= new DoubleLinkedList<VOBlock>();
		
		
		Iterator<VOTrip> iTrip= trips.iterator();
		while(iTrip.hasNext()) 
		{
			VOTrip trip=iTrip.next();
			VOBlock b= new VOBlock(trip.getBlockId(),trip.getRouteId());
			if(r.existElement(b)) 
			{
				try 
				{
					b.addTrip(trip);
				} catch (Exception e) 
				{
					
				}
				 
			}
			else 
			{
				b.addTrip(trip);
				r.add(b);
			}
		}
		
		
		ordenarViajes(r);
		return r;
	}
	
	public void ordenarViajes(DoubleLinkedList<VOBlock> rlista) 
	{
		Iterator<VOBlock> iterador= rlista.iterator();
		while(iterador.hasNext()) 
		{
			VOBlock actual = iterador.next();
			actual.doHeapSort();
		}
		
	}
	
	public void generarColaPrioridad() 
	{
		Iterator<VOBlock> iterador = resumenList.iterator();
		while(iterador.hasNext()) 
		{
			colaPrioridad.enqueue(iterador.next());
			
		}
		
	}
	
	public void heapSortIdentificadoresViaje() 
	{
		
	}
	
	public DoubleLinkedList<VOBlock> listaRutasPrioridad()
	{
		DoubleLinkedList<VOBlock> lista = new DoubleLinkedList<VOBlock>();
		while(!colaPrioridad.isEmpty()) 
		{
			lista.addAtEnd(colaPrioridad.dequeueElementoMayor());
		}
		return lista;
	}
	
	
	
	
	public void loadTrips(String tripsFile) 
	{

	
		String cadena;

		FileReader file = null;
		BufferedReader reader = null;

		try
		{
			file= new FileReader(tripsFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					String csvLineFields[] = cadena.split(",");

					int routeId = getIntFromCsvLine(csvLineFields, 0);
					int serviceId = getIntFromCsvLine(csvLineFields, 1);
					int tripId = getIntFromCsvLine(csvLineFields, 2);
					String tripHeadsign = getStringFromCsvLine(csvLineFields, 3);
					String tripShortName = getStringFromCsvLine(csvLineFields, 4);
					int directionId = getIntFromCsvLine(csvLineFields, 5);
					int blockId = getIntFromCsvLine(csvLineFields, 6);
					int shapeId = getIntFromCsvLine(csvLineFields, 7);
					int wheelchairAccessible = getIntFromCsvLine(csvLineFields, 8);
					int bikesAllowed = getIntFromCsvLine(csvLineFields, 9);

					VOTrip newTrip = new VOTrip(routeId, serviceId, tripId, tripHeadsign, tripShortName, directionId, blockId, shapeId, wheelchairAccessible, bikesAllowed);

					trips.addAtEnd(newTrip); // al final para preservar el orden del archivo
				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}
		
		trips.doMergeSort();
		
		
	}

	
	private String getStringFromCsvLine(String[] csvLineFields, int index) {
		return csvLineFields[index].trim();
	}

	private int getIntFromCsvLine(String[] csvLineFields, int index) {
		return Integer.parseInt(getStringFromCsvLine(csvLineFields, index));
	}

	private Integer getNullableIntFromCsvLine(String[] csvLineFields, int index) {
		int number = 0;
		try 
		{
			number = getStringFromCsvLine(csvLineFields, index).isEmpty() ? null : getIntFromCsvLine(csvLineFields, index);
		} catch (Exception e) {

		}
		return number;
	}




	public PriorityQueue<VOBlock> getColaPrioridad() {
		return colaPrioridad;
	}




	public void setColaPrioridad(PriorityQueue<VOBlock> colaPrioridad) {
		this.colaPrioridad = colaPrioridad;
	}




	public DoubleLinkedList<VOTrip> getTrips() {
		return trips;
	}




	public void setTrips(DoubleLinkedList<VOTrip> trips) {
		this.trips = trips;
	}




	public DoubleLinkedList<VOBlock> getResumenList() {
		return resumenList;
	}




	public void setResumenList(DoubleLinkedList<VOBlock> resumenList) {
		this.resumenList = resumenList;
	}
	
	

	

}
