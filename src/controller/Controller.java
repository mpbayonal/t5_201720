package controller;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VOBlock;


public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static STSManager  manager = new STSManager();
	

		
	public static void loadTrips() 
	{
		
		manager.loadTrips("data/trips.txt");
	}
	
	public static void generarTablaResumen() throws Exception 
	{
		
		manager.punto6();
	}
	public static void generarColaPrioridad() throws Exception 
	{
		
		manager.generarColaPrioridad();
	}
	public static DoubleLinkedList<VOBlock> darListaOrdenadaPrioridad() throws Exception 
	{
		
		return manager.listaRutasPrioridad();
	}


}
