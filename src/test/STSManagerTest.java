package test;

import java.io.File;
import java.util.Iterator;

import junit.framework.TestCase;

import model.logic.STSManager;

public class STSManagerTest extends TestCase {

	private STSManager stsManager;
	
	protected void setUp() throws Exception {
		stsManager = new STSManager();
	}

	
	
	public void testLoadTrips() throws Exception {

		stsManager.loadTrips("./data/test/trips_test.txt");
		assertEquals("Se cargaron los elementos del archivo Trips",stsManager.getTrips().getSize().intValue(), 1050);
	}
	

}
